#!/bin/sh -x
make

# remove the generated graph files
rm fine-tune.c.137t.gml
rm fine-tune.c.137t.gml-calc_max_distance.gml
rm fine-tune.c.137t.gml-calc_max_distance_recursive.gml
rm fine-tune.c.137t.gml-depth_first_search.gml
rm fine-tune.c.137t.gml-fine_tune_cfg.gml
rm fine-tune.c.137t.gml-is_ancestor.gml
rm fine-tune.c.137t.gml-mark_edge.gml
rm fine-tune.c.137t.gml-mark_edges.gml
rm fine-tune.c.137t.gml-search.gml

./gcctree2gml fine-tune.c.137t.nrv fine-tune.c.137t.gml

