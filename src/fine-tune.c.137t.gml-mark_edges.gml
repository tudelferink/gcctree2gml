# generated by gcctree2gml from fine-tune.c.137t.nrv for function mark_edges()
graph [
  directed 1
  node [ id 80 label "mark_edges()
ENTRY" ]
  node [ id 81 label "mark_edges()
EXIT" ]
  node [ id 40 label "<bb 2>: mark_edges()
  current_function.6D.5580_2 = current_functionD.4113;  
  cfgD.4211_3 = current_function.6D.5580_2->cfgD.4162;  
  eD.4212_4 = cfgD.4211_3->edgeD.4159;  
  if (eD.4212_4 != 0B)  
    goto <bb 3>;  
  else  
    goto <bb 7>;  
" ]
  node [ id 41 label "<bb 3>: mark_edges()
   
" ]
  node [ id 42 label "<bb 7>: mark_edges()
  return;  
" ]
  node [ id 43 label "<bb 4>: mark_edges()
  D.5581_5 = eD.4212_13->typeD.4133;  
  if (D.5581_5 == 0)  
    goto <bb 5>;  
  else  
    goto <bb 6>;  
" ]
  node [ id 44 label "<bb 6>: mark_edges()
  eD.4212_6 = eD.4212_13->nextD.4136;  
  if (eD.4212_6 != 0B)  
    goto <bb 4>;  
  else  
    goto <bb 7>;  
" ]
  node [ id 45 label "<bb 5>: mark_edges()
  mark_edgeD.4204 (eD.4212_13);  
" ]
  edge [ source 80 target 40 ]
  edge [ source 40 target 41 ]
  edge [ source 40 target 42 ]
  edge [ source 41 target 43 ]
  edge [ source 44 target 43 graphics [ fill "#0000ff" ] ]
  edge [ source 43 target 45 ]
  edge [ source 43 target 44 ]
  edge [ source 45 target 44 ]
  edge [ source 44 target 42 ]
  edge [ source 42 target 81 ]
]
# 8 nodes and 10 edges