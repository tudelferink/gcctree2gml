# generated with gcctree2gml
graph [
  directed 1
  node [ id 72 label "search()
ENTRY" ]
  node [ id 73 label "search()
EXIT" ]
  node [ id 3 label "<bb 2>: search()
  bbD.4175_2(D)->visitedD.4144 = 1;  
  veD.4179_3 = bbD.4175_2(D)->succD.4149;  
  if (veD.4179_3 != 0B)  
    goto <bb 3>;  
  else  
    goto <bb 7>;  
" ]
  node [ id 4 label "<bb 3>: search()
   
" ]
  node [ id 5 label "<bb 7>: search()
  count.10D.5607_8 = countD.4173;  
  bbD.4175_2(D)->dfs_orderD.4145 = count.10D.5607_8;  
  count.11D.5608_10 = count.10D.5607_8 + -1;  
  countD.4173 = count.11D.5608_10;  
  return;  
" ]
  node [ id 6 label "<bb 4>: search()
  eD.4180_4 = veD.4179_9->edgeD.4138;  
  succ_bbD.4178_5 = eD.4180_4->targetD.4135;  
  D.5604_6 = succ_bbD.4178_5->visitedD.4144;  
  if (D.5604_6 == 0)  
    goto <bb 5>;  
  else  
    goto <bb 6>;  
" ]
  node [ id 7 label "<bb 6>: search()
  veD.4179_7 = veD.4179_9->nextD.4139;  
  if (veD.4179_7 != 0B)  
    goto <bb 4>;  
  else  
    goto <bb 7>;  
" ]
  node [ id 8 label "<bb 5>: search()
  eD.4180_4->typeD.4133 = 1;  
  searchD.4176 (succ_bbD.4178_5);  
" ]
  edge [ source 72 target 3 ]
  edge [ source 3 target 4 ]
  edge [ source 3 target 5 ]
  edge [ source 4 target 6 ]
  edge [ source 7 target 6 fill "#0000ff" ]
  edge [ source 6 target 8 ]
  edge [ source 6 target 7 ]
  edge [ source 8 target 7 ]
  edge [ source 7 target 5 ]
  edge [ source 5 target 73 ]
]
